import React, {useContext} from 'react';
import { Outlet } from 'react-router-dom';
import SignIn from '../Pages/SignIn';
import { MyContext } from "../Services/MyContext";

const UserRoute = () => {
    const {user} = useContext(MyContext);
    let type = user?.userType;
    return (type === "ADMIN" || type === "USER" ? <Outlet/> : <SignIn/>);
};

export default UserRoute;