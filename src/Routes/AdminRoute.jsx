import React, {useContext} from 'react';
import { Outlet } from 'react-router-dom';
import UserPanel from '../Pages/UserPanel';
import { MyContext } from "../Services/MyContext";


const AdminRoute = () => {
    const {user} = useContext(MyContext);
    let type = user?.userType;
    return (type === "ADMIN" ? <Outlet/> : <UserPanel/>);
};

export default AdminRoute;