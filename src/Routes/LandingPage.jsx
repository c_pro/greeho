import { BrowserRouter, Routes, Route } from "react-router-dom";
import Footer from "../Components/Footer";
import NavBar from "../Components/NavBar";
import Error from "../Error/Error";
import Graph from "../Pages/Graph";
import Home from "../Pages/Home";
import SignIn from "../Pages/SignIn";
import Signup from "../Pages/Signup";
import UserPanel from "../Pages/UserPanel";
import AdminRoute from "./AdminRoute";
import UserRoute from "./UserRoute";

const LandingPage = () => {
    return (
        <BrowserRouter>
            <NavBar />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="signin" element={<SignIn />} />
                <Route path="signup" element={<Signup />} />
                {/* using protected route for user and admin */}
                <Route element={<UserRoute />}>
                    <Route path="userPanel" element={<UserPanel />} />
                    <Route element={<AdminRoute />}>
                        <Route path="graph" element={<Graph />} />
                    </Route>
                </Route>
                <Route path="*" element={<Error />} />
            </Routes>
            <Footer />
        </BrowserRouter>
    );
};

export default LandingPage;
