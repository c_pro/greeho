import Styles from "./Home.module.scss";

const Home = () => {
    return (
        <div className={Styles.main}>
           <h1>Welcome to GREEHO</h1>
        </div>
    );
};

export default Home;