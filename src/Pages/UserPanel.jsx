import React, { useEffect, useState } from "react";
import Styles from "./UserPanel.module.scss";
import InstanceWithHeader from "../Services/InstanceWithHeader";
import Configuration from "../Services/Configuration";
import useToggler from "../Hooks/useToggler";
import UpdateUser from "./UpdateUser";
import { FaUserCircle } from "react-icons/fa";

const UserPanel = () => {
    const [myInfo, setMyInfo] = useState();
    const [message, setMessage] = useState("");
    const [viewEdit, setViewEdit] = useToggler();


    useEffect(() => {
        //only getting value if edit card not in viewport
        if (!viewEdit) {
            async function getSelf() {
                try {
                    const response = await InstanceWithHeader.get(
                        Configuration.BACKEND_SELF
                    );
                    if (response.data.status === 200) {
                        setMessage("");
                         console.log(response.data.data.account);
                        setMyInfo(response.data.data.account);
                    }
                } catch (error) {
                    if (
                        error.response.data.message &&
                        error.response.data.status === 500
                    ) {
                        setMessage(error.response.data.message);
                    }
                }
            }
            getSelf();
        }
    }, [viewEdit]);

    return (
        <div className={Styles.main}>
            <h2 className={Styles.title}>My info</h2>
            <div className={Styles.picContainer} style={viewEdit ? {display: "none"} : {}}>
                {(myInfo?.profilePicture) ? (
                    <img
                        className={Styles.pic}
                        key={new Date()}
                        src={`https://exam.greeho.com/api/files/${myInfo?.profilePicture}`}
                        alt="pic"
                        
                    />
                ) : (
                    <FaUserCircle className={Styles.pic} />
                )}
            </div>

            <p className={Styles.txt}>{message}</p>
            <br />
            {!viewEdit ? (
                <div
                    style={!myInfo ? { display: "none" } : {}}
                    className={Styles.form}
                >
                    <p style={{ width: "100%" }}>Username</p>
                    <p className={Styles.data}>{myInfo?.username}</p>
                    <p style={{ width: "100%" }}>Mobile</p>
                    <p className={Styles.data}>{myInfo?.mobile}</p>
                    <p style={{ width: "100%" }}>Email</p>
                    <p className={Styles.data}>{myInfo?.email}</p>
                    <p style={{ width: "100%" }}>Marital Status</p>
                    <p className={Styles.data}>{myInfo?.maritalStatus}</p>
                    <p style={{ width: "100%" }}>Address</p>
                    <p className={Styles.data}>{myInfo?.address}</p>
                    <p style={{ width: "100%" }}>University</p>
                    <p className={Styles.data}>{myInfo?.university}</p>
                    <p style={{ width: "100%" }}>Weight</p>
                    <p className={Styles.data}>{myInfo?.weight}</p>
                    <p style={{ width: "100%" }}>Height</p>
                    <p className={Styles.data}>{myInfo?.height}</p>
                    <button
                        style={myInfo === undefined ? { display: "none" } : {}}
                        onClick={setViewEdit}
                    >
                        Edit
                    </button>
                </div>
            ) : (
                <>
                    {" "}
                    <UpdateUser myInformation={myInfo} />
                    <button onClick={setViewEdit}>Cancel</button>
                </>
            )}

            <br />
            <br />
            <br />
        </div>
    );
};

export default UserPanel;
