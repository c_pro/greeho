import React, { useEffect, useState } from "react";
import { Line } from "react-chartjs-2";
import { Chart as ChartJS } from "chart.js/auto";
import InstanceWithHeader from "../Services/InstanceWithHeader";
import Configuration from "../Services/Configuration";
import Styles from "./Graph.module.scss";
import { Link } from "react-router-dom";


const Graph = () => {
    const [graphData, setGraphData] = useState();
    const [message, setMessage] = useState();

    //setting viewing options for the graph 
    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: "top",
            },
            title: {
                display: true,
                text: "Greeho Price Chart",
            },
        },
    };

    //getting the data from server
    useEffect(() => {
        async function getChartData() {
            try {
                const response = await InstanceWithHeader.get(
                    Configuration.BACKEND_GRAPH
                );
                if (response.data.status === 200) {
                    setMessage("");
                   // console.log(response.data.data.graphVertices);

                   //setting up the value according to the requirements
                    setGraphData({
                        labels: response.data.data.graphVertices.map((data) =>
                            data.dateCreated.slice(0, 10)
                        ),
                        datasets: [
                            {
                                label: "Last Traded Price",
                                data: response.data.data.graphVertices.map(
                                    (data) => data.lastTradedPrice
                                ),
                                borderColor: "rgb(255, 99, 132)",
                                backgroundColor: "rgba(255, 99, 132, 0.5)",
                                borderWidth: 1.5,
                            },
                            {
                                label: "Closing Price",
                                data: response.data.data.graphVertices.map(
                                    (data) => data.closingPrice
                                ),
                                borderColor: "rgb(53, 162, 235)",
                                backgroundColor: "rgba(53, 162, 235, 0.5)",
                                borderWidth: 1.5,
                            },
                        ],
                    });
                }
            } catch (error) {
                if (
                    error.response.data.message &&
                    error.response.data.status === 500
                ) {
                    setMessage(error.response.data.message);
                } 
            }
        }
        getChartData();
    }, []);

    return (
        <div className={Styles.main}>
            <p className={Styles.txt}>{message}</p>
            <div className={Styles.container}>
                {graphData && (
                    <Line
                        className={Styles.graph}
                        data={graphData}
                        options={options}
                    />
                )}
               
            </div>
            <br />
            <div className={Styles.box}>
                
                 <Link className={Styles.link} to="/">
                Back to home
            </Link>
            </div>
           
        </div>
    );
};

export default Graph;
