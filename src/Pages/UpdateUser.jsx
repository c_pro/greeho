import React, { useState } from "react";
import Styles from "./UpdateUser.module.scss";
import InstanceWithHeader from "../Services/InstanceWithHeader";
import Configuration from "../Services/Configuration";
import useToggler from "../Hooks/useToggler";

const UpdateUser = ({ myInformation }) => {
    const [myInfo, setMyInfo] = useState(myInformation);
    const [view, setView] = useToggler();
    const [errMsg, setErrMsg] = useState();
    const [message, setMessage] = useState("");
    const profilePicUploadorUpdate = myInformation?.profilePicture;

    //update user info
    const updateUserInfo = async (event) => {
        event.preventDefault();
        try {
            const response = await InstanceWithHeader.put(
                Configuration.BACKEND_SELF,
                myInfo
            );
            if (response.data.status === 200) {
                setView();
                setErrMsg();
                setMessage(response.data.message);
                // console.log(response.data);
            }
        } catch (e) {
            if (e.response.data.message) {
                setMessage(e.response.data.message);
            }
            //storing err message
            if (e.response.data.data.errors) {
                setErrMsg(e.response.data.data.errors);
            } else {
                setErrMsg();
            }
        }
    };
    //showing error msgs
    const errMsgShow = errMsg?.map((msg, index) => {
        return (
            <div key={index}>
                <p className={Styles.txt}>
                    {Object.keys(msg)} - {Object.values(msg)}
                </p>
            </div>
        );
    });

    //uploading profile picture
    const uploadProfilePic = async (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);

        //uploading the picture file to the server
        InstanceWithHeader.post(Configuration.BACKEND_FILE, formData)
            .then(function (response) {
                console.log(response);
                if (response.data.status === 200) {
                    setErrMsg();
                    setMessage(response.data.message);
                    //console.log(response.data.data.fileName);
                    myInfo.profilePicture = response.data.data.fileName;
                }
            })
            .then(() => {
                //updating self with the profile picture
                InstanceWithHeader.put(Configuration.BACKEND_SELF, myInfo).then(
                    function (response) {
                        if (response.data.status === 200) {
                            setView();
                            setErrMsg();
                            setMessage(response.data.message);
                            // console.log(response.data);
                        }
                    }
                );
            })
            .catch(function (error) {
                if (error.response.data.message) {
                    setMessage(error.response.data.message);
                }
                //storing err message
                if (error.response.data.data.errors) {
                    setErrMsg(error.response.data.data.errors);
                } else {
                    setErrMsg();
                }
            });
    };

    //updating the profilePicture
    const updateProfilePicture = async (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        try {
            const response = await InstanceWithHeader.put(
                Configuration.BACKEND_FILE + "/" + myInformation.profilePicture,
                formData
            );
            if (response.data.status === 200) {
                setView();
                setErrMsg();
                setMessage(response.data.message);
                //console.log(response.data);
            }
        } catch (error) {}
    };

    //deleting profile picture
    const deleteProfilePicture = async () => {
       
        //deleting the file itself
        InstanceWithHeader.delete(
            Configuration.BACKEND_FILE + "/" + myInformation.profilePicture
        )
            .then(function (response) {
                if (response.data.status === 200) {
                    setErrMsg();
                    setMessage(response.data.message);
                    myInfo.profilePicture = "";
                }
            })
            .then(() => {
                InstanceWithHeader.put(Configuration.BACKEND_SELF, myInfo).then(
                    (response) => {
                        if (response.data.status === 200) {
                            setView();
                            setMessage(response.data.message);
                        }
                    }
                );
            })
            .catch(function (error) {
                setMyInfo({ ...myInfo, profilePicture: "" });
                try {
                    if (error.response.data.message) {
                        setMessage(error.response.data.message);
                    }
                    //if self is not updated but file is deleated
                    if (
                        error.response.data.message ===
                        "Requested file was not found."
                    ) {
                        myInfo.profilePicture = "";
                        InstanceWithHeader.put(
                            Configuration.BACKEND_SELF,
                            myInfo
                        )
                            .then((response) => {
                                if (response.data.status === 200) {
                                    console.log("function ran");
                                    setView();
                                    setMessage(response.data.message);
                                    // console.log(response.data);
                                }
                            })
                            .catch((error) => {
                                console.log("with error");
                            });
                    }
                    //storing err message
                    if (error.response.data.data.errors) {
                        setErrMsg(error.response.data.data.errors);
                    } else {
                        setErrMsg();
                    }
                } catch (error) {}
            });
    };

    return (
        <div className={Styles.main}>
            <p className={Styles.txt}>{message}</p>
            {errMsgShow}
            <br />
            <div style={profilePicUploadorUpdate ? { display: "none" } : {}}>
                <form
                    style={view ? { display: "none" } : {}}
                    onSubmit={uploadProfilePic}
                >
                    <h3>Upload profile picture</h3>
                    <br />
                    <input type="file" name="file" />
                    <br />
                    <br />
                    <div className={Styles.btnContainer}>
                        <button type="submit">Submit</button>
                        <button onClick={deleteProfilePicture}>Delete</button>
                    </div>
                </form>
            </div>
            <br />
            <div style={!profilePicUploadorUpdate ? { display: "none" } : {}}>
                <form
                    style={view ? { display: "none" } : {}}
                    onSubmit={updateProfilePicture}
                >
                    <h3>Update profile picture</h3>
                    <br />
                    <input type="file" name="file" />
                    <br />
                    <br />
                    <div className={Styles.btnContainer}>
                        <button type="submit">Update</button>
                        <button onClick={deleteProfilePicture}>Delete</button>
                    </div>
                </form>
            </div>

            <br />
            <form
                style={view ? { display: "none" } : {}}
                className={Styles.form}
                onSubmit={updateUserInfo}
            >
                <p style={{ width: "100%" }}>Username</p>
                <input
                    type="text"
                    name="username"
                    placeholder={myInfo?.username}
                    value={myInfo?.username}
                    onChange={(event) => {
                        setMyInfo({ ...myInfo, username: event.target.value });
                    }}
                />
                <p style={{ width: "100%" }}>Mobile</p>
                <input
                    type="mobile"
                    name="mobile"
                    placeholder={myInfo?.mobile}
                    value={myInfo?.mobile}
                    onChange={(event) => {
                        setMyInfo({ ...myInfo, mobile: event.target.value });
                    }}
                />
                <p style={{ width: "100%" }}>Email</p>
                <input
                    type="email"
                    name="email"
                    placeholder={myInfo?.email}
                    value={myInfo?.email}
                    onChange={(event) => {
                        setMyInfo({ ...myInfo, email: event.target.value });
                    }}
                />
                <p style={{ width: "100%" }}>Marital Status</p>
                <input
                    type="text"
                    name="maritalStatus"
                    placeholder={myInfo?.maritalStatus}
                    value={myInfo?.maritalStatus}
                    onChange={(event) => {
                        setMyInfo({
                            ...myInfo,
                            maritalStatus: event.target.value,
                        });
                    }}
                />
                <p style={{ width: "100%" }}>Address</p>
                <input
                    type="text"
                    name="address"
                    placeholder={myInfo?.address}
                    value={myInfo?.address}
                    onChange={(event) => {
                        setMyInfo({ ...myInfo, address: event.target.value });
                    }}
                />
                <p style={{ width: "100%" }}>University</p>
                <input
                    type="text"
                    name="university"
                    placeholder={myInfo?.university}
                    value={myInfo?.university}
                    onChange={(event) => {
                        setMyInfo({
                            ...myInfo,
                            university: event.target.value,
                        });
                    }}
                />
                <p style={{ width: "100%" }}>Weight</p>
                <input
                    type="text"
                    name="weight"
                    placeholder={myInfo?.weight}
                    value={myInfo?.weight}
                    onChange={(event) => {
                        setMyInfo({ ...myInfo, weight: event.target.value });
                    }}
                />
                <p style={{ width: "100%" }}>Height</p>
                <input
                    type="text"
                    name="height"
                    placeholder={myInfo?.height}
                    value={myInfo?.height}
                    onChange={(event) => {
                        setMyInfo({ ...myInfo, height: event.target.value });
                    }}
                />
                <button type="submit">Submit</button>
            </form>
        </div>
    );
};

export default UpdateUser;
