import React, { useLayoutEffect, useRef, useState, useContext  } from "react";
import Styles from "./Signup.module.scss";
import { Link, useNavigate  } from "react-router-dom";
import Instance from "../Services/Instance";
import Configuration from "../Services/Configuration";
import { MyContext } from "../Services/MyContext";


const Signup = () => {
    const { setUser } = useContext(MyContext);
    let navigate = useNavigate();
    const [message, setMessage] = useState();
    const [errMsg, setErrMsg] = useState();
    const regRef = useRef();

    //getting focus on input
    useLayoutEffect(() => {
        regRef.current.focus();
    }, []);

    //registration handler function
    const signUpHandler = async (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);

        try {
            const response = await Instance.post(
                Configuration.BACKEND_REGISTRATION,
                formData
            );
            if (response.data.status === 200) {
               // console.log(response.data.data.user);
               setUser(response.data.data.user);
                setErrMsg();
                setMessage(response.data.message);
                localStorage.setItem("token", response.data.data.accessToken);
                localStorage.setItem(
                    "refreshToken",
                    response.data.data.refreshToken
                );
                navigate("/userPanel");
            }
        } catch (e) {
            if (e.response.data.message) {
                setMessage(e.response.data.message);
            }
            //storing err message
            if (e.response.data.data.errors) {
                setErrMsg(e.response.data.data.errors);
            } else {
                setErrMsg();
            }
        }
    };

    //showing error msgs
    const errMsgShow = errMsg?.map((msg, index) => {
        return (
            <div key={index}>
                <p className={Styles.txt}>
                    {Object.keys(msg)} - {Object.values(msg)}
                </p>
            </div>
        );
    });

    return (
        <div className={Styles.main}>
            <img className={Styles.logo} src="" alt="" />
            <h2 className={Styles.title}>Sign Up</h2>
            <br />
            <p className={Styles.txt}>{message}</p>
            {errMsgShow}
            <br />
            <form className={Styles.form} onSubmit={signUpHandler}>
                <input
                    ref={regRef}
                    type="text"
                    name="username"
                    placeholder="Enter your username"
                    required
                />
                <input
                    type="email"
                    name="email"
                    placeholder="Enter your email"
                    required
                />
                <input
                    type="mobile"
                    name="mobile"
                    placeholder="Enter your mobile no"
                />
                <input
                    type="text"
                    name="address"
                    placeholder="Enter your address"
                />
                <input
                    type="text"
                    name="university"
                    placeholder="Enter your university"
                />
                <input
                    type="text"
                    name="weight"
                    placeholder="Enter your weight"
                />
                <input
                    type="text"
                    name="height"
                    placeholder="Enter your height"
                />
                <input
                    type="password"
                    name="password"
                    placeholder="Enter your password"
                />
                <input
                    type="password"
                    name="confirmPassword"
                    placeholder="Confirm your password"
                />

                <button type="submit">Submit</button>
            </form>
            <br />
            <p>
                already a member?{" "}
                <Link className={Styles.link} to="/signIn">
                    Login
                </Link>
            </p>
            <br />
            <Link className={Styles.linkCal} to="/">
                Back to home
            </Link>
        </div>
    );
};

export default Signup;
