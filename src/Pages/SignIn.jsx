import React, { useContext } from "react";
import { useLayoutEffect, useRef, useState } from "react";
import Styles from "./SignIn.module.scss";
import { Link, useNavigate } from "react-router-dom";
import Instance from "../Services/Instance";
import Configuration from "../Services/Configuration";
import { MyContext } from "../Services/MyContext";

const SignIn = () => {
    const { setUser } = useContext(MyContext);
    const [message, setMessage] = useState();
    const [errMsg, setErrMsg] = useState();
    const loginRef = useRef();
    let navigate = useNavigate();


    //getting focus on input
    useLayoutEffect(() => {
        loginRef.current.focus();
    }, []);

    //login handler function
    const loginHandler = async (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);

        try {
            const response = await Instance.post(
                Configuration.BACKEND_LOGIN_PATH,
                formData
            );
            if (response.data.status === 200) {
               // console.log(response.data.data.user);
                setUser(response.data.data.user);
                setErrMsg();
                setMessage(response.data.message);
                localStorage.setItem("token", response.data.data.accessToken);
                localStorage.setItem(
                    "refreshToken",
                    response.data.data.refreshToken
                );
                navigate("/userPanel")
            }
        } catch (e) {
            if (e.response.data.message) {
                setMessage(e.response.data.message);
            }
            //storing err message
            if (e.response.data.data.errors) {
                setErrMsg(e.response.data.data.errors);
            } else {
                setErrMsg();
            }
        }
    };

    //showing error msgs
    const errMsgShow = errMsg?.map((msg, index) => {
        return (
            <div>
                <p className={Styles.txt} key={index}>
                    {Object.keys(msg)} - {Object.values(msg)}
                </p>
            </div>
        );
    });
    return (
        <div className={Styles.main}>
            <img className={Styles.logo} src="" alt="" />
            <h2 className={Styles.title}>Sign In</h2>
            <br />
            <p className={Styles.txt}>{message}</p>
            {errMsgShow}
            <br />
            <form className={Styles.form} onSubmit={loginHandler}>
                <input
                    ref={loginRef}
                    type="text"
                    placeholder="Enter your username"
                    name="username"
                    required
                />
                <input
                    type="password"
                    placeholder="Enter your password"
                    name="password"
                />
                <button type="submit">Submit</button>
            </form>
            <br />
            <br />
            <p>
                not a member?{" "}
                <Link className={Styles.link} to="/signup">
                    SignUp
                </Link>
            </p>
            <br />
            <Link className={Styles.linkCal} to="/">
                Back to home
            </Link>
        </div>
    );
};

export default SignIn;
