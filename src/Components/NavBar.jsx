import React, { useContext } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { MyContext } from "../Services/MyContext";
import Styles from "./NavBar.module.scss";

const NavBar = () => {
    const { user, setUser } = useContext(MyContext);
    let view = user?.userType;
    let navigate = useNavigate();

    //user log out handler
    function logout() {
        localStorage.clear();
        setUser();
        navigate("/");
    }
    return (
        <div className={Styles.main}>
            <div className={Styles.left}>
                <NavLink
                    className={Styles.logo}
                    style={({ isActive }) =>
                        isActive ? { textDecoration: "none" } : undefined
                    }
                    to="/"
                >
                    Greeho
                </NavLink>
            </div>
            <div className={Styles.right}>
                <div style={view ? { display: "none" } : {}}>
                    <NavLink
                        className={Styles.link}
                        style={({ isActive }) =>
                            isActive
                                ? { textDecoration: "underLine" }
                                : undefined
                        }
                        to="/signin"
                    >
                        Login
                    </NavLink>
                </div>
                <div style={!view ? { display: "none" } : {}}>
                    <NavLink
                        className={Styles.link}
                        style={({ isActive }) =>
                            isActive
                                ? { textDecoration: "underLine" }
                                : undefined
                        }
                        to="/userPanel"
                    >
                        Profile
                    </NavLink>
                </div>
                <div style={view === "ADMIN" ? {} : { display: "none" }}>
                    <NavLink
                        className={Styles.link}
                        style={({ isActive }) =>
                            isActive
                                ? { textDecoration: "underLine" }
                                : undefined
                        }
                        to="/graph"
                    >
                        Graph
                    </NavLink>
                </div>
                <p
                    className={Styles.link}
                    onClick={logout}
                    style={!view ? { display: "none" } : {}}
                >
                    Logout
                </p>
            </div>
        </div>
    );
};

export default NavBar;
