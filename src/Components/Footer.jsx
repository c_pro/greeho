import React from 'react';
import Styles from "./Footer.module.scss";

const Footer = () => {
    return (
        <div className={Styles.main}>
           <p> © copyright {new Date().getFullYear()}</p> 
        </div>
    );
};

export default Footer;