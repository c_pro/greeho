const Configuration = {
    /** API version. */
    API_VERSION: "0.1.0",

    /** Base URL of backend server. */
    BACKEND_BASE_URL: "https://exam.greeho.com",

    /**  Backend api key. */
    BACKEND_API_KEY:
        "N9hH7M65SqpFl26gdJ8DKPo32ppr85kFAfkb8u8x39NOD729hd20PoE1Wccx0O16",

    /**  URL to create new user. */
    BACKEND_REGISTRATION: "/api/users/register",

    /**  URL to login user. */
    BACKEND_LOGIN_PATH: "/api/users/login",

    /**  URL to get self. */
    BACKEND_SELF: "/api/users/self",

    /**  renew token. */
    BACKEND_TOKEN_RENEWAL_PATH: "/api/tokens/renew",

    /**  backend graph data. */
    BACKEND_GRAPH: "/api/graphVertices",

    /**  backend files. */
    BACKEND_FILE: "/api/files",
};

export default Configuration;
