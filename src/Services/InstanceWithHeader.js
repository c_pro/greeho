import axios from "axios";
import Configuration from "./Configuration";
import jwt_decode from "jwt-decode";
import dayjs from "dayjs";

const InstanceWithHeader = axios.create({
    baseURL: Configuration.BACKEND_BASE_URL,
    headers: {
        "x-api-key": Configuration.BACKEND_API_KEY,
    },
});

InstanceWithHeader.interceptors.request.use(
    async (req) => {
        let authToken = localStorage.getItem("token");
        const user = jwt_decode(authToken);
        const isExpired = dayjs.unix(user.exp).diff(dayjs()) < 1;

        if (!isExpired) {
            req.headers.Authorization = `Bearer ${authToken}`;
            return req;
        } else {
            //refreshing the token
            try {
                let configHeader = {
                    headers: {
                        Authorization: `bearer ${localStorage.getItem(
                            "token"
                        )}`,
                        "Authorization-Refresh": `bearer ${localStorage.getItem(
                            "refreshToken"
                        )}`,
                        "x-api-key": Configuration.BACKEND_API_KEY,
                    },
                };
                const newTokenResponse = await axios.put(
                    Configuration.BACKEND_BASE_URL +
                        Configuration.BACKEND_TOKEN_RENEWAL_PATH,
                    {},
                    configHeader
                );

                if (newTokenResponse.data.status === 200) {
                    localStorage.setItem(
                        "token",
                        newTokenResponse.data.data.accessToken
                    );
                    localStorage.setItem(
                        "refreshToken",
                        newTokenResponse.data.data.refreshToken
                    );
                    //attaching the new token with the existing request
                    req.headers.Authorization = `Bearer ${newTokenResponse.data.data.accessToken}`;
                    return req;
                }
            } catch (error) {}
        }
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    }
);



export default InstanceWithHeader;
