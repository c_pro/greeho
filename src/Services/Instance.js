import axios from "axios";
import Configuration from "./Configuration";

const Instance = axios.create({  
    baseURL: Configuration.BACKEND_BASE_URL,
    headers: { "x-api-key" : Configuration.BACKEND_API_KEY },
});



export default Instance;
