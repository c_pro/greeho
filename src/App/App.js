import LandingPage from "../Routes/LandingPage";
import { useEffect, useState } from "react";
import InstanceWithHeader from "../Services/InstanceWithHeader";
import Configuration from "../Services/Configuration";
import { MyContext } from "../Services/MyContext";

function App() {
    const [retry, setRetry] = useState(false);
    const [user, setUser] = useState({ });

    useEffect(() => {
        if (localStorage.getItem("token") === null) {
            return;
        } else {
            async function getSelf() {
                try {
                    const response = await InstanceWithHeader.get(
                        Configuration.BACKEND_SELF
                    );
                    if (response.data.status === 200) {
                        //console.log(response.data.data.account);
                        setUser(response.data.data.account);
                    }
                } catch (error) {
                    try {
                        if (
                            error.response.data.message &&
                            error.response.data.status === 500
                        ) {
                            setRetry(true);
                        }
                    } catch (error) {}
                }
            }
            getSelf();
        }
    }, [retry]);

    return (
        <MyContext.Provider
            value={{
                user,
                setUser,
            }}
        >
            <LandingPage />
        </MyContext.Provider>
    );
}

export default App;
